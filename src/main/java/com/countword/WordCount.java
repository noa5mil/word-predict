package com.countword;
import java.io.IOException;
import java.util.StringTokenizer;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper.Context;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.Partitioner;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.mapreduce.lib.input.SequenceFileInputFormat;

public class WordCount {
    public static class MapClass extends Mapper<LongWritable, Text, Text, LongWritable> {

        public void map(LongWritable key, Text value, Context context) throws IOException,  InterruptedException {
            String[] splitted = value.toString().split("\t");
            if (splitted.length < 4) { return; } /* malformed line, skip it. */
            String ngram = splitted[0];
            String count = splitted[2];
            if (!ngram.matches("[א-ת\\s]+")){
                return;
            }
            String[] ngram_words = ngram.split(" ");
            if(ngram_words.length > 3){
                return;
            }
            for (String word : ngram_words){
                if (word.equals("")){
                    return;
                }
            }
            if (ngram_words.length == 1) {
                context.write(new Text("*"), new LongWritable(Long.valueOf(count)));
            }
            //System.out.println("mapper send key: " + ngram + " value: " + count);
            context.write(new Text(ngram), new LongWritable(Long.valueOf(count)));
        }
    }

    public static class ReduceClass extends Reducer<Text,LongWritable,Text,LongWritable> {
        public void reduce(Text key, Iterable<LongWritable> values, Context context) throws IOException,  InterruptedException {
            long sum = 0;
            for (LongWritable value : values) {
                sum += value.get();
            }
            //System.out.println("reducer send key: " + key.toString() + " value: " + sum);
            context.write(key, new LongWritable(sum));
        }


    }

    public static class PartitionerClass extends Partitioner<Text, LongWritable> {
        @Override
        public int getPartition(Text key, LongWritable value, int numPartitions) {
            return Math.abs(key.hashCode()) % numPartitions;
        }
    }

    public static void main(String[] args) throws Exception {

        Configuration conf = new Configuration();
        Job job = new Job(conf, "word count");
        job.setJarByClass(WordCount.class);
        job.setMapperClass(MapClass.class);
        job.setPartitionerClass(PartitionerClass.class);
        job.setCombinerClass(ReduceClass.class);
        job.setReducerClass(ReduceClass.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(LongWritable.class);
        job.setInputFormatClass(SequenceFileInputFormat.class);
        FileInputFormat.addInputPath(job, new Path(args[0]));
        FileOutputFormat.setOutputPath(job, new Path(args[1]));
        System.exit(job.waitForCompletion(true) ? 0 : 1);
    }

}

